from .base import FunctionalTest

class LayoutAndStyling(FunctionalTest):
    def test_layout_and_styling(self):
        # Mary goes to the homepage
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1280, 768)

        # She notices that the page title is nicely centered
        title = self.browser.find_element_by_tag_name('h1')
        self.assertAlmostEqual(
            title.location['x'] + title.size['width'] / 2,
            512,
            delta=3
        )
