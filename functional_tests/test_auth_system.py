from .base import FunctionalTest
from selenium import webdriver

class AuthSystemTest(FunctionalTest):
    def test_user_signup_login_and_logout(self):
        # Mary hears about a new play-money prediction market site.

        # She decides to visit the home page, and sees that it mentions
        # predictions.
        self.browser.get(self.live_server_url)

        self.assertIn("Predict", self.browser.title)

        # On the page, she sees a form titled "sign up" and a form titled
        # "log in"

        page_text = self.browser.find_element_by_tag_name('body').text

        self.assertIn("Login", page_text)
        self.assertIn("SignUp", page_text)

        self.assertTrue(self.browser.find_element_by_id('login_form'))
        self.assertTrue(self.browser.find_element_by_id('signup_form'))

        # When Mary tries to log in with her usual username and password, she
        # sees an error message come up
        login_username_box = self.browser.find_element_by_xpath(
            "//form[@id='login_form']//input[@name='username']"
        )
        login_password_box = self.browser.find_element_by_xpath(
            "//form[@id='login_form']//input[@name='password']"
        )
        login_submit_btn = self.browser.find_element_by_xpath(
            "//form[@id='login_form']//input[@name='submit']"
        )

        login_username_box.send_keys("mary")
        login_password_box.send_keys("lmgtfy1234")
        login_submit_btn.click()

        page_text = self.browser.find_element_by_tag_name('body').text

        self.assertIn('Please enter a correct username and password', page_text)
        # She realizes she hasn't signed up yet, so fills in her information
        # into the signup form and submits it.
        self.browser.get(self.live_server_url)

        signup_username_box = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='username']"
        )
        signup_password1_box = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='password1']"
        )
        signup_password2_box = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='password2']"
        )
        signup_submit_button = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='submit']"
        )

        signup_username_box.send_keys("mary")
        signup_password1_box.send_keys("lmgtfy1234")
        signup_password2_box.send_keys("lmgtfy1234")
        signup_submit_button.click()

        # She is redirected to the home page and sees, not login or signup
        # forms, but a logout button
        page_text = self.browser.find_element_by_tag_name('body').text

        self.assertNotIn('Login', page_text)
        self.assertNotIn('SignUp', page_text)
        self.assertIn('Log out', page_text)

        # Mary decides that's enough of that, and presses it, before closing
        # her browser
        self.browser.find_element_by_id('logout').click()
        self.browser.quit()

        # Now Margaret too hears about the site, visits, and sees similar
        # information to Mary
        self.browser = webdriver.Firefox()
        self.browser.get(self.live_server_url)

        self.assertIn("Predict", self.browser.title)

        page_text = self.browser.find_element_by_tag_name('body').text

        self.assertIn("Login", page_text)
        self.assertIn("SignUp", page_text)

        self.assertTrue(self.browser.find_element_by_id('login_form'))
        self.assertTrue(self.browser.find_element_by_id('signup_form'))

        # She tries to sign up, but Mary has taken her username already, and she
        # sees an error message to that effect
        signup_username_box = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='username']"
        )
        signup_password1_box = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='password1']"
        )
        signup_password2_box = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='password2']"
        )
        signup_submit_button = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='submit']"
        )

        signup_username_box.send_keys("mary")
        signup_password1_box.send_keys("lmgtfy1234")
        signup_password2_box.send_keys("lmgtfy1234")
        signup_submit_button.click()

        # She signs up with a different username, and all is well this time.
        signup_username_box = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='username']"
        )
        signup_password1_box = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='password1']"
        )
        signup_password2_box = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='password2']"
        )
        signup_submit_button = self.browser.find_element_by_xpath(
            "//form[@id='signup_form']//input[@name='submit']"
        )

        signup_username_box.send_keys("marge")
        signup_password1_box.send_keys("lmgtfy1234")
        signup_password2_box.send_keys("lmgtfy1234")
        signup_submit_button.click()

        # She goes back to the home page, and this time no login or signup forms
        # appear, and the page greets her by her username
        page_text = self.browser.find_element_by_tag_name('body').text

        self.assertNotIn('Login', page_text)
        self.assertNotIn('SignUp', page_text)
        self.assertIn('Log out', page_text)

        # She logs out, and now Mary logs in on the same browser.
        self.browser.find_element_by_id('logout').click()

        page_text = self.browser.find_element_by_tag_name('body').text

        self.assertIn('Login', page_text)
        self.assertIn('SignUp', page_text)
        self.assertNotIn('Log out', page_text)

        login_username_box = self.browser.find_element_by_xpath(
            "//form[@id='login_form']//input[@name='username']"
        )
        login_password_box = self.browser.find_element_by_xpath(
            "//form[@id='login_form']//input[@name='password']"
        )
        login_submit_btn = self.browser.find_element_by_xpath(
            "//form[@id='login_form']//input[@name='submit']"
        )

        login_username_box.send_keys("mary")
        login_password_box.send_keys("lmgtfy1234")
        login_submit_btn.click()

        page_text = self.browser.find_element_by_tag_name('body').text

        self.assertNotIn('Login', page_text)
        self.assertNotIn('SignUp', page_text)
        self.assertIn('Log out', page_text)

        # Satisfied that it's all working, they both go out for drinks to
        # celebrate