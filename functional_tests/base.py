from django.test import LiveServerTestCase
from selenium import webdriver
from pyvirtualdisplay import Display

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        self.display = Display()
        self.display.start()
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(4)

    def tearDown(self):
        self.browser.quit()
        self.display.stop()
