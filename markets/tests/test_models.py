from django.core.exceptions import ValidationError
from django.test import TestCase
from django.contrib.auth.models import User

from math import exp, log
from decimal import Decimal as Dc

from markets.models import Market, MarketAccount

class MarketAccountsTest(TestCase):
    def test_users_create_market_accounts(self):
        user = User.objects.create_user(username="mary", email="mary@example.com", password="flibertigibbet")
        self.assertTrue(user.marketaccount)
        self.assertEqual(user.marketaccount.balance, 200)

class MarketsTest(TestCase):
    def test_market(self):
        # A new user creates a market
        patron = User.objects.create_user(username="mary", email="mary@example.com", password="flibertigibbet")
        market = Market.objects.create_market(name="Will this test pass?",
                                              description="This is a market to determine whether this test will pass.",
                                              patron=patron.marketaccount,
                                              seed_fund=50,
                                             )
        patron_bet = market.bets.all()[0]

        # A new user bets on the market
        bettor_1 = User.objects.create_user(username="marg", email="marg@example.com", password="flibertigibbet")
        bet_1 = market.create_bet(owner=bettor_1.marketaccount, choices={'True':0.2, 'False':0.8})

        self.assertAlmostEqual(bet_1.cost(), market.b_value*log(0.5/0.2))
        self.assertAlmostEqual(bettor_1.marketaccount.balance, 200-bet_1.cost())

        # So does a second
        bettor_2 = User.objects.create_user(username="chris", email="chris@example.com", password="flibertigibbet")
        bet_2 = market.create_bet(owner=bettor_2.marketaccount, choices={'True':0.8, 'False':0.2})

        self.assertAlmostEqual(bet_2.cost(), market.b_value*log(0.8/0.2))
        self.assertAlmostEqual(bettor_2.marketaccount.balance, 200-bet_2.cost())

        # And a third
        bettor_3 = User.objects.create_user(username="marc", email="marc@example.com", password="whatever")
        bet_3 = market.create_bet(owner=bettor_3.marketaccount, choices={'True':0.1, 'False':0.9})

        self.assertAlmostEqual(bet_3.cost(), market.b_value*log(0.8/0.1))
        self.assertAlmostEqual(bettor_3.marketaccount.balance, 200-bet_3.cost())

        # Verifying that scores make sense
        self.assertAlmostEqual(patron_bet.score(market.choices.get(name="True")), market.b_value*log(50/10))
        self.assertAlmostEqual(bet_1.score(market.choices.get(name="True")), market.b_value*log(0.2/0.5))
        self.assertAlmostEqual(bet_2.score(market.choices.get(name="True")), market.b_value*log(0.8/0.2))
        self.assertAlmostEqual(bet_3.score(market.choices.get(name="True")), market.b_value*log(0.1/0.8))

        # The patron closes the market
        market.close(choice=market.choices.get(name='True'))
