from django.test import TestCase
from django.core.urlresolvers import resolve, reverse
from django.contrib.auth.models import User
from markets.models import Market
from markets.views import market_account_detail

class MarketViewTestCase(TestCase):
    def set_up_and_log_in_user(self, username, password):
        User.objects.create_user(username, "%s@example.com" % (username), password)
        post_data = {
            'username':username,
            'password':password,
        }
        return self.client.post(reverse('login'), data=post_data)

class MarketAccountPageTest(MarketViewTestCase):
    def test_market_account_page_resolves_correct_url(self):
        self.set_up_and_log_in_user('mary', 'lmgtfy')
        found = resolve('/markets/account/')
        self.assertEqual(found.func, market_account_detail)

    def test_market_account_page_contains_balance(self):
        self.set_up_and_log_in_user('mary', 'password')

        page = self.client.get(reverse('market_account_detail'))

        self.assertContains(page, '200')

class MarketListViewTest(MarketViewTestCase):
    pass