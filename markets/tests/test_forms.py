from django.test import TestCase
from django.contrib.auth.models import User

from markets.forms import *
from markets.models import Market

class FormTestCase(TestCase):
    pass

class MarketCreateFormTest(FormTestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="maria", password="janet")
        self.existing_market = Market.objects.create_market(
            name="Exists already",
            description="whatever",
            patron=self.user.marketaccount,
            seed_fund=50
        )

    def test_form_accepts_valid_input(self):
        data = {
                'name':'Is this market going to work?',
                'description':'Blah blah blah lorem ipsum whatever',
                'seed_fund':50
               }

        market_create_form = MarketCreateForm(data=data)
        self.assertTrue(market_create_form.is_valid())

    def test_form_rejects_long_name(self):
        data = {
                'name':'Is this market going to work? I do not know. However, if we discuss this overmuch we run the risk of...',
                'description':'Blah blah blah lorem ipsum whatever',
                'seed_fund':50
               }

        market_create_form = MarketCreateForm(data=data)
        self.assertFalse(market_create_form.is_valid())

    def test_form_rejects_existing_market_name(self):
        data = {
                'name':'Exists already',
                'description':'asfasgartrgsv',
                'seed_fund':50
               }

        market_create_form = MarketCreateForm(data=data)
        self.assertFalse(market_create_form.is_valid())


class ChoiceCreateFormTest(FormTestCase):
    def test_form_accepts_valid_input(self):
        data = {'name':'The test fails', 'probability':0.5}
        choice_create_form = ChoiceCreateForm(data=data)

        self.assertTrue(choice_create_form.is_valid())

    def test_form_rejects_zero_probability(self):
        data= {'name':'The test fails', 'probability':0.0}
        choice_create_form = ChoiceCreateForm(data=data)

        self.assertFalse(choice_create_form.is_valid())

    def test_form_rejects_probability_one(self):
        data = {'name':'The test fails', 'probability':1.0}
        choice_create_form = ChoiceCreateForm(data=data)

        self.assertFalse(choice_create_form.is_valid())


class ChoicesCreateFormsetTest(FormTestCase):
    def test_form_accepts_valid_input(self):
        data = {
            'form-TOTAL_FORMS': u'3',
            'form-INITIAL_FORMS': u'0',
            'form-MAX_NUM_FORMS': u'',
            'form-0-name':'The test fails',
            'form-0-probability':0.6,
            'form-1-name':'The test passes',
            'form-1-probability':0.1,
            'form-2-name':'The test sucks',
            'form-2-probability':0.3,
            }

        choices_create_formset = ChoicesCreateFormset(data=data)

        self.assertTrue(choices_create_formset.is_valid())

    def test_form_rejects_total_probability_not_one(self):
        data = {
            'form-TOTAL_FORMS': u'3',
            'form-INITIAL_FORMS': u'0',
            'form-MAX_NUM_FORMS': u'',
            'form-0-name':'The test fails',
            'form-0-probability':0.6,
            'form-1-name':'The test passes',
            'form-1-probability':0.3,
            'form-2-name':'The test sucks',
            'form-2-probability':0.3, # adds to 1.2
            }

        choices_create_formset = ChoicesCreateFormset(data=data)

        self.assertFalse(choices_create_formset.is_valid())

    def test_form_rejects_duplicate_choices(self):
        data = {
            'form-TOTAL_FORMS': u'3',
            'form-INITIAL_FORMS': u'0',
            'form-MAX_NUM_FORMS': u'',
            'form-0-name':'The test fails',
            'form-0-probability':0.6,
            'form-1-name':'The test passes',
            'form-1-probability':0.1,
            'form-2-name':'The test fails', # duplicated name
            'form-2-probability':0.3,
            }

        choices_create_formset = ChoicesCreateFormset(data=data)

        self.assertFalse(choices_create_formset.is_valid())


class MarketEditFormTest(FormTestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="maria", password="janet")
        self.market = Market.objects.create_market(name="whatever",
                                                   description="whatever",
                                                   patron=self.user.marketaccount,
                                                   seed_fund=50
                                                  )

    def test_form_accepts_valid_input(self):
        data = {'description':'This is a description of a market'}

        market_edit_form = MarketEditForm(data=data, instance=self.market)
        self.assertTrue(market_edit_form.is_valid())

class MarketCloseFormTest(FormTestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="maria", password="janet")
        self.market = Market.objects.create_market(name="whatever",
                                                   description="whatever",
                                                   patron=self.user.marketaccount,
                                                   seed_fund=50
                                                  )

    def test_market_close_form_accepts_valid_input(self):
        data = {'answer':'True'}

        market_close_form = MarketCloseForm(data=data, market=self.market)
        self.assertTrue(market_close_form.is_valid())

    def test_market_close_form_rejects_invalid_input(self):
        data = {'answer':'Nonsense'}

        market_close_form = MarketCloseForm(data=data, market=self.market)
        self.assertFalse(market_close_form.is_valid())

class BetCreateFormTest(FormTestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="maria", password="janet")
        self.market = Market.objects.create_market(name="whatever",
                                                   description="whatever",
                                                   patron=self.user.marketaccount,
                                                   seed_fund=50
                                                  )

    def test_bet_create_form_has_correct_fields(self):
        bet_create_form = BetCreateForm(market=self.market)
        print(bet_create_form.as_p())

    def test_bet_create_form_processes_data(self):
        data = {'True':0.8, 'False':0.2}
        bet_create_form = BetCreateForm(market=self.market, data=data)

        self.assertTrue(bet_create_form.is_valid())

    def test_bet_create_form_rejects_invalid_fields(self):
        data = {'True':0.8, 'False':0.1, 'Other':0.1}
        bet_create_form = BetCreateForm(market=self.market, data=data)

        bet_create_form.is_valid()

        print(bet_create_form.cleaned_data)

