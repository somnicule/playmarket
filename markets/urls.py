from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    #View market account
    url(r'^account/$',
        'markets.views.market_account_detail',
        name='market_account_detail'),

    # List markets
    url(r'^$',
        'markets.views.market_list',
        name='market_list'),

#     Create market
    url(r'^create/$',
        'markets.views.market_create',
        name='market_create'),

    # View market
    url(r'^(?P<market_id>[\d]+)/$',
        'markets.views.market_detail',
        name='market_detail'),

    # Edit market
    url(r'^(?P<market_id>[\d]+)/edit/$',
        'markets.views.market_edit',
        name='market_edit'),

    # Close market
    url(r'^(?P<market_id>[\d]+)/close/$',
        'markets.views.market_close',
        name='market_close'),


    # Create bet
    url(r'^(?P<market_id>[\d]+)/create_bet/$',
        'markets.views.bet_create',
        name='bet_create'),
)
