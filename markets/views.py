from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.generic import DetailView, ListView

from markets.models import Market
from markets.forms import MarketCreateForm, ChoicesCreateFormset, MarketEditForm, MarketCloseForm, BetCreateForm
from markets.forms import process_market_creation

@login_required
def market_account_detail(request):
    context_dict = {
        'balance':request.user.marketaccount.balance,
        'market_list':request.user.marketaccount.markets,
        'bet_list':request.user.marketaccount.bets,
    }
    return render(request, 'market_account.html', context_dict)


def market_list(request):
    market_list = Market.objects.filter(status=Market.OPEN)
    paginator = Paginator(market_list, 5)
    page = request.GET.get('page')
    try:
        markets = paginator.page(page)
    except PageNotAnInteger:
        markets = paginator.page(1)
    except EmptyPage:
        markets = paginator.page(paginator.num_pages)
    context_dict = {
        'markets':markets
    }
    return render(request, 'market_list.html', context_dict)

@login_required
def market_create(request):
    if request.method=='POST':
        market_create_form = MarketCreateForm(data=request.POST)
        choices_create_formset = ChoicesCreateFormset(data=request.POST)
        if market_create_form.is_valid() and choices_create_formset.is_valid():
            market = process_market_creation(request=request,
                                    market_create_form=market_create_form,
                                    choices_create_formset=choices_create_formset
                                   )
            return redirect(reverse('market_detail', args=(market.id,)))

    else:
        market_create_form = MarketCreateForm()
        choices_create_formset = ChoicesCreateFormset()

    context_dict = {
        'market_create_form': market_create_form,
        'choices_create_formset':choices_create_formset,
        'title':"Create Your Market",
    }

    return render(request, 'market_create.html', context_dict)

def market_detail(request, market_id):
    market = Market.objects.get(id=market_id)
    latest_bet = market.bets.all().reverse()[0]

    context_dict = {
                    'market':market,
                    'latest_bet':latest_bet
                   }

    return render(request, 'market_detail.html', context_dict)

@login_required
def market_edit(request, market_id):
    market = Market.objects.get(id=market_id)
    if request.user != market.patron.user:
        return redirect(reverse('home_page'))

    if request.method=='POST':
        market_edit_form = MarketEditForm(data=request.POST, instance=market)
        if market_edit_form.is_valid():
            market_edit_form.save()
            return redirect(reverse('market_detail', args=(market_id,)))
    else:
        market_edit_form = MarketEditForm()

    context_dict = {
                    'market':market,
                    'market_edit_form':market_edit_form,
                    'title':"Edit market"
                   }

    return render(request, 'market_edit.html', context_dict)

def market_close(request, market_id):
    market = Market.objects.get(id=market_id)
    if request.user != market.patron.user:
        return redirect(reverse('home_page'))

    if request.method=='POST':
        market_close_form = MarketCloseForm(data=request.POST, market=market)
        if market_close_form.is_valid():
            market.close(market_close_form.cleaned_data['answer'])
            return redirect(reverse('market_detail', args=(market_id,)))
    else:
        market_close_form = MarketCloseForm(market=market)

    context_dict = {
                    'market':market,
                    'market_close_form':market_close_form,
                    'title':"Close market"
                   }
    return render(request, 'market_close.html', context_dict)

def bet_create(request, market_id):
    market = Market.objects.get(id=market_id)
    if request.user == market.patron.user:
        return redirect(reverse('home_page'))

    if request.method == 'POST':
        bet_create_form = BetCreateForm(data=request.POST, market=market)
        if bet_create_form.is_valid():
            market.create_bet(choices=bet_create_form.cleaned_data, owner=request.user.marketaccount)
            return redirect(reverse('market_detail', args=(market_id,)))
    else:
        bet_create_form = BetCreateForm(market=market)

    context_dict = {
                    'market':market,
                    'bet_create_form':bet_create_form,
                    'title':"Create bet on %s" % (market.name)
                    }
    return render(request, 'bet_create.html', context_dict)

