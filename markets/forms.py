from django import forms
from django.forms.formsets import formset_factory, BaseFormSet

from markets.models import Market, Choice

class MarketCreateForm(forms.Form):
    """Form for the creation of a market. Does not include choices and initial
      probabilities.

    Attributes:
      name (CharField): The title of the prediction market.
      description (TextArea): A longer description of the terms of the market.
      seed_fund (IntegerField): The initial liquidity fund for the market.
    """
    name = forms.CharField(max_length=40)
    description = forms.CharField(widget=forms.Textarea)
    seed_fund = forms.IntegerField()

    def clean_name(self):
        """Ensure that name field is unique."""
        name = self.cleaned_data['name']
        if any(Market.objects.filter(name=name)):
            raise forms.ValidationError("A market with that name already exists.")
        return name


class ChoiceCreateForm(forms.Form):
    """Form to create a single choice.

    Attributes:
      name (CharField): The name for a single choice
      probability (DecimalField): The initial probability estimate for that
        choice.
    """
    name = forms.CharField(max_length=30)
    probability = forms.DecimalField(min_value=0.01, max_value=0.99, max_digits=2, decimal_places=2)


class BaseChoicesCreateFormset(BaseFormSet):
    """BaseFormSet for the ChoicesCreateFormset."""
    def clean(self):
        """Ensure choice names are unique, and probabilities sum to 1."""
        if any(self.errors):
            return

        names = []
        total = 0

        for form in self.forms:
            if form.has_changed():
                total += form.cleaned_data['probability']
                if form.cleaned_data['name'] in names:
                    raise forms.ValidationError("All choices must have unique names.")
                names.append(form.cleaned_data['name'])

        if total != 1.0:
            raise forms.ValidationError("The total probability must be one.")


ChoicesCreateFormset = formset_factory(ChoiceCreateForm, formset=BaseChoicesCreateFormset, extra=5)


class MarketEditForm(forms.ModelForm):
    """ModelForm to allow editing of Market description."""
    class Meta:
        model = Market
        fields = ['description',]


class MarketCloseForm(forms.Form):
    """Form to close a market after the outcome has been determined by the
      creator.

    Attributes:
      answer (ModelChoiceField): A selection of the market's model choices.
    """
    answer = forms.ModelChoiceField(queryset=Choice.objects.none(), to_field_name='name')

    def __init__(self, *args, **kwargs):
        """Set answer queryset to the Market choices."""
        market = kwargs.pop('market')
        super(MarketCloseForm, self).__init__(*args, **kwargs)

        self.fields['answer'].queryset=market.choices.all()


class BetCreateForm(forms.Form):
    """A form to submit probabilities for the choices in creating a new bet."""
    def __init__(self, *args, **kwargs):
        """Populate the fields with DecimalFields for each of the choices in the
          market."""
        self.market = kwargs.pop('market')
        super(BetCreateForm, self).__init__(*args, **kwargs)

        for choice in self.market.choices.all():
            self.fields[choice.name] = forms.DecimalField(min_value=0.01, max_value=0.99, max_digits=2, decimal_places=2)

    def clean(self):
        """Verify that the probabilities sum to one."""
        cleaned_data = super(BetCreateForm, self).clean()
        total = 0
        for prob in cleaned_data.values():
            total += prob

        if total != 1.0:
            raise forms.ValidationError("The total probability must be one.")

        return cleaned_data


def process_market_creation(request, market_create_form, choices_create_formset):
    """Process MarketCreateForm and ChoicesCreateFormset together.
      Helper function."""
    if market_create_form.is_valid() and choices_create_formset.is_valid():
        choices = {}
        for form in choices_create_formset:
            if form.has_changed():
                choices[form.cleaned_data['name']] = form.cleaned_data['probability']

        return Market.objects.create_market(name=market_create_form.cleaned_data['name'],
                                     description=market_create_form.cleaned_data['description'],
                                     seed_fund=market_create_form.cleaned_data['seed_fund'],
                                     patron=request.user.marketaccount,
                                     choices=choices,
                                    )
