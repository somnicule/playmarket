from django.db import models

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User

from math import log

from decimal import Decimal as Dc


class MarketAccount(models.Model):
    """Model to store balance information for a given user.
    Attrributes:
      user (OneToOneField): The user associated with the MarketAccount
      balance (DecimalField): The balance of the market account.
    """
    user = models.OneToOneField(User)
    balance = models.DecimalField(max_digits=7, decimal_places=2, default=200)

    def add_funds(self, quantity):
        """Increase balance of a MarketAccount by the specified amount."""
        self.balance = self.balance + quantity
        self.save()

    def remove_funds(self, quantity):
        """Decrease balance of a MarketAccount by the specified amount. No
        verification of available balance."""
        self.balance = self.balance - quantity
        self.save()

    def __str__(self):
        return u'Account of user: %s' % (self.user.username)


class MarketManager(models.Manager):
    """Custom manager with one new method for the creation of markets."""
    def create_market(self, name, description, patron, seed_fund, choices={'True':Dc(0.5),'False':Dc(0.5)}):
        """Create a new market and corresponding choice using the detailed inputs.

        Args:
          name (string): A title for the market, max 60 chars.
          description (string): Details regarding how the outcome of the market
            should be determined, no character limit.
          patron (MarketAccount): The user that is creating the market and paying
            for the seed fund, with permissions to close the market.
          seed_fund (decimal): A decimal value for how much should be removed
            from the patron's balance, to provide liquidity to the market.
          choices (dict, optional): A dict of choice titles as strings, and
            corresponding starting probabilities as decimals. Defaults to
              {'True':Dc(0.5),'False':Dc(0.5)}

            Recommended to have an "other" option, or something similar, if the
            list of outcomes is not fully exclusive.

        Returns:
          Market: The newly created Market object.

        Raises:
          TODO: Add exceptions for insufficient funds, invalid inputs, etc.
        """
        b_value = seed_fund/log(1/min(choices.values()))

        new_market = Market(name=name,
                            description=description,
                            patron=patron,
                            seed_fund=seed_fund,
                            b_value=b_value,
                           )

        new_market.save()

        patron.remove_funds(seed_fund)

        for name in choices.keys():
            Choice(market=new_market, name=name).save()

        initial_bet = Bet(owner=new_market.patron, market=new_market, is_initial=True)
        initial_bet.save()
        for name, probability in choices.items():
            probability = Probability(choice=new_market.choices.get(name=name),
                                      bet=initial_bet,
                                      probability=probability
                                     ).save()
        return new_market


class Market(models.Model):
    """A model to manage all aspects of a particular prediction market.

    Attributes:
      name (CharField): A title for the market, max 60 chars.
      description (TextField): Details regarding how the outcome of the market
        should be determined, no character limit.
      patron (ForeignKey): The user that is creating the market and paying
        for the seed fund, with permissions to close the market.
      seed_fund (DecimalField): A decimal value for how much has been removed
            from the patron's balance, to provide liquidity to the market.
      b_value (DecimalField): The liquidity parameter used in calculations of
        bet costs and payoffs.
      answer (OneToOneField): The field of the eventual selected choice, blank
        until the market has been closed.
      status (CharField): A selection of choices (Open, Closed, Null) used to
        display if the market is accepting bets, closed with a definite outcome,
        or null with an indeterminate outcome respectively.
    """
    OPEN = "OP"
    CLOSED = "CL"
    NULL = "NL"
    STATUS_CHOICES = (
        (OPEN, "Open"),
        (CLOSED, "Closed"),
        (NULL, "Null"),
    )

    name = models.CharField(max_length=60)
    description = models.TextField()
    patron = models.ForeignKey(MarketAccount, related_name="markets")
    seed_fund = models.DecimalField(max_digits=10,
                                    decimal_places=2,
                                    default=20,
                                    )

    b_value = models.DecimalField(max_digits=8, decimal_places=4)
    answer = models.OneToOneField('Choice', blank=True, null=True, related_name="market_answer")
    status = models.CharField(max_length=4,
                              choices=STATUS_CHOICES,
                              default=OPEN,
                              )
    objects = MarketManager()

    def create_bet(self, owner, choices):
        """Create a new bet on the market.

        Args:
          owner (MarketAccount): The market account corresponding to the user
            placing the bet.
          choices (dict): A dict of choice titles as strings, and
            corresponding probabilities as decimals.

            Note: No verification done within the method to ensure the correct
              number of bets are made. All validation should occur in form
              processing.

        Returns:
          Bet: The new bet that has just been created.
        """
        new_bet = Bet(owner=owner, market=self)
        new_bet.save()
        for name, probability in choices.items():
            new_probability = Probability(bet=new_bet,
                                          choice=self.choices.get(name=name),
                                          probability=probability
                                         )
            new_probability.save()
        owner.remove_funds(new_bet.cost())
        return new_bet

    def close(self, choice):
        """Close the market, and settle all bets

        Args:
          choice (Choice): The choice out of the ones belonging to the market
            that the owner has decided is the correct outcome.
        """
        for bet in self.bets.all():
            bet.close(choice)
        self.status=self.CLOSED
        self.answer=choice
        self.save()

    def render_status(self):
        """Return a string to display the status in a user-readable format.

        Returns:
          string: The string that corresponds to the answer to the betting market.
        """
        if self.status != self.CLOSED:
            if self.status == self.OPEN:
                return "Open"
            elif self.status == self.NULL:
                return "Null"
        else:
            return self.answer.name

    def __str__(self):
        return u'%s' % (self.name)


class Choice(models.Model):
    """An option for the outcome a prediction market is betting on.

    Atrributes:
      market (ForeignKey): The associated Market object.
      name (CharField): A maximum of 40 chars describing the choice.
    """
    market = models.ForeignKey(Market, related_name="choices")
    name = models.CharField(max_length=40)

    def __str__(self):
        return u'%s' % (self.name)


class Bet(models.Model):
    """

    Atrributes:
      owner (ForeignKey): The MarketAccount associated with the user placing the bet.
      market (ForeignKey): The Market the bet is being placed on.
      is_initial (bool): A boolean for the case in which the bet is initial, which has
    """
    owner = models.ForeignKey(MarketAccount, related_name='bets')
    market = models.ForeignKey(Market, related_name='bets')
    is_initial = models.BooleanField(default=False)

    class Meta:
        ordering = ['id']

    def score(self, choice):
        """Calculate the score of a bet given a particular outcome.

        Args:
          choice (Choice): The outcome for which the score is to be calculated

        Returns:
          decimal: The score of the bet given the choice.
        """
        if self.is_initial:
            denom = self.market.bets.all().reverse()[0].probabilities.get(choice=choice).probability
            numer = self.probabilities.get(choice=choice).probability
        else:
            numer = self.probabilities.get(choice=choice).probability
            denom = self.market.bets.filter(id__lt=self.id).reverse()[0].probabilities.get(choice=choice).probability

        return self.market.b_value * Dc(log(numer/denom))

    def cost(self):
        """Calculate the maximum loss of the bet, i.e. the cost of placing it.

        Returns:
          A positive Decimal value, the amount to be removed from the bettors
            MarketAccount.
        """
        if self.is_initial:
            return self.market.seed_fund
        else:
            scores = []
            for choice in self.market.choices.all():
                scores.append(self.score(choice))
            return -min(scores)

    def close(self, choice):
        """Refund the appropriate amount to the bettor."""
        self.owner.add_funds(self.cost())
        self.owner.add_funds(self.score(choice))

    def __str__(self):
        return u'%s on %s' % (self.owner.user.username, self.market.name)


class Probability(models.Model):
    """The specific probability for a given choice and bet.

    Attributes:
      bet (ForeignKey): The associated Bet object.
      choice (Foreignkey): The associated Choice object.
      probability (DecimalField): The probability for that Choice and Bet.
    """
    bet = models.ForeignKey(Bet, related_name="probabilities")
    choice = models.ForeignKey(Choice, related_name="probabilities")
    probability = models.DecimalField(max_digits=3,
                                      decimal_places=3,
                                     )

    def render_prob(self):
        """Return a string representing the probability as a percentage."""
        return "%s%%" % (str(self.probability * 100))

    def __str__(self):
        return u'%s, %s, %s' % (self.bet, self.choice, self.probability)


@receiver(post_save)
def create_market_account_for_user(sender, **kwargs):
    """Create a market account when a new user is registered."""
    if sender == User and kwargs.get('created'):
        MarketAccount(user=kwargs.get('instance')).save()