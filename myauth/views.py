from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout

def home_page(request):
    user = request.user

    if request.user.is_authenticated():
        logged_in=True
    else:
        logged_in=False

    data = {
        'signup_form':UserCreationForm(),
        'login_form':AuthenticationForm(),
        'logged_in':logged_in,
    }
    return render(request, 'home.html', data)

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password1']
            )
            login(request, user)
            return redirect(home_page)
    else:
        form = UserCreationForm()
    data = {'form':form}
    return render(request, 'registration/signup.html', data)
