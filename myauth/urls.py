from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'myauth.views.home_page', name='home_page'),
    url(r'^signup/$', 'myauth.views.signup', name='signup'),
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),

    url(r'^logout/$',
        'django.contrib.auth.views.logout',
        {'next_page':'home_page'},
        name='logout'
        ),
    # url(r'^blog/', include('blog.urls')),
)
