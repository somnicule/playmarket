from django.core.urlresolvers import resolve, reverse
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.test import TestCase

from myauth.views import home_page, signup

class AuthViewsTestCase(TestCase):
    def create_and_login_user(self, username, password):
        User.objects.create_user(username, "%s@example.com" % (username), password)
        post_data = {
            'username':username,
            'password':password,
        }
        return self.client.post(reverse('login'), data=post_data)


class HomePageTest(AuthViewsTestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')

        self.assertEqual(found.func, home_page)


class SignupTest(AuthViewsTestCase):
    def test_signup_redirects_after_POST(self):
        post_data = {
            'username':'mary',
            'password1':'lmgtfy',
            'password2':'lmgtfy',
        }
        response = self.client.post(reverse('signup'), data=post_data)

        self.assertEqual(response.status_code, 302)


    def test_signup_creates_user_from_POST(self):
        initial_count = User.objects.all().count()
        post_data = {
            'username':'mary',
            'password1':'lmgtfy',
            'password2':'lmgtfy',
        }
        self.client.post(reverse('signup'), data=post_data)

        self.assertEqual(User.objects.all().count()-initial_count, 1)
        self.assertTrue(User.objects.get(username='mary'))


    def test_signup_logs_in_user_after_POST(self):
        post_data = {
            'username':'mary',
            'password1':'lmgtfy',
            'password2':'lmgtfy',
        }
        self.client.post(reverse('signup'), data=post_data)

        self.assertIn('_auth_user_id', self.client.session)

class LoginTest(AuthViewsTestCase):
    def test_login_redirects_to_home_after_POST(self):
        response = self.create_and_login_user("mary", "lmgtfy")

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home_page'))

    def test_login_logs_in_user_after_POST(self):
        self.create_and_login_user("mary", "lmgtfy")

        self.assertIn('_auth_user_id', self.client.session)

class LogoutTest(AuthViewsTestCase):
    def test_logout_redirects_to_home(self):
        self.create_and_login_user("mary", "lmgtfy")

        response = self.client.get(reverse('logout'))

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home_page'))

    def test_logout_logs_user_out(self):
        self.create_and_login_user("mary", "lmgtfy")

        self.client.get(reverse('logout'))

        self.assertNotIn('_auth_user_id', self.client.session)