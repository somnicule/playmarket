from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'playmarket.views.home', name='home'),
    url(r'^', include('myauth.urls')),
    url(r'^markets/', include('markets.urls')),

    # url(r'^admin/', include(admin.site.urls)),
)
